##
## Makefile for Makefile in /home/marion_t/My_FTL For My_FTL
##
## Made by MARION Tristan
## Login   <marion_t@etna-alternance.net>
##
## Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
## Last update Mon Nov  6 11:10:41 2017 MARION Tristan
##

CC = gcc
RM = rm -f
RMDIR = rm -fr
MKDIR = mkdir
BUILD_PATH = obj
NAME = my_ftl
SRC = src/main.c \
	src/air_shed.c \
	src/container.c \
	src/system_control.c \
	src/system_repair.c \
	src/my_string.c \
	src/readline.c \
	src/bonus.c \
	src/game.c \
	src/libmy.c \
	src/display.c \
	src/enemy.c \
	src/free.c \
	src/color.c \
	src/commands/jump.c \
	src/commands/stat.c \
	src/commands/detect.c \
	src/commands/attack.c

OBJS = $(SRC:%.c=$(BUILD_PATH)/%.o)

INCLUDES = -I ./includes -I ../includes
CFLAGS = -W -Wextra -Werror -Wall -pedantic $(INCLUDES)

.PHONY	: all dirs clean fclean re

all		: $(NAME)

$(NAME) : dirs $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(NAME)

dirs	:
	$(MKDIR) -p $(dir $(OBJS))

$(BUILD_PATH)/%.o :	%.c
	$(CC) $(CFLAGS) -c $< -o $@

clean	:
	$(RM) $(OBJS)
	$(RMDIR) $(dir $(OBJS))

fclean	: clean
	$(RM) $(NAME)

re: fclean all

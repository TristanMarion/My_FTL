/*
** color.c for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
*/

#include "../includes/includes.h"
#include        <stdlib.h>

const char *reset_color = "\033[0m";

typedef struct s_color t_color;

struct s_color
{
	char *color;
	char *unicode;
};

static const t_color g_color[] = {{"clear",   "\033[H\033[2J"},
								  {"red",     "\033[31m"},
								  {"green",   "\033[32m"},
								  {"yellow",  "\033[33m"},
								  {"blue",    "\033[34m"},
								  {"magenta", "\033[35m"},
								  {"cyan",    "\033[36m"},
								  {NULL,      NULL}};

void	my_putstr_color(const char *color, const char *str)
{
	int	i;

	i = 0;
	while (g_color[i].color != NULL &&
		   (my_strcmp(g_color[i].color, color) != 0))
		i++;
	if (g_color[i].color == NULL)
	{
		my_putstr(str);
		return;
	}
	my_putstr(g_color[i].unicode);
	my_putstr(str);
	my_putstr(reset_color);
}

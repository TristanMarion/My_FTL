/*
** enemy.c for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
*/

#include "../includes/enemy.h"
#include "../includes/game.h"

int	enemy_jump(t_game *game)
{
	int	rand;

	rand = random() % 100;
	if (rand < 30)
	{
		if ((game->enemy = create_enemy()) == NULL)
			return (-1);
		else
			return (1);
	}
	return (0);
}

t_enemy			*create_enemy()
{
	static int	nb_enemy = 0;
	t_enemy		*enemy;
	int			i;

	i = 0;
	if ((enemy = malloc(sizeof(t_enemy))) == NULL)
		return (NULL);
	enemy->damage = 10;
	enemy->hp = 20;
	while (i < nb_enemy)
	{
		enemy->damage = (int) (enemy->damage * 1.5);
		enemy->hp = (int) (enemy->hp * 1.5);
		i++;
	}
	nb_enemy++;
	return enemy;
}

void free_enemy(t_game *game)
{
	if (game->enemy != NULL)
		free(game->enemy);
}

/*
** free.c for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
*/

#include "../includes/game.h"
#include "../includes/container.h"

void free_ship(t_ship *ship)
{
	free(ship->weapon->system_state);
	free(ship->weapon);
	free(ship->ftl_drive->system_state);
	free(ship->ftl_drive);
	free(ship->navigation_tools->system_state);
	free(ship->navigation_tools);
	while (ship->container->first)
		del_freight_from_container(ship, ship->container->first);
	free(ship->container);
	free(ship);
}

void free_game(t_game *game)
{
	free_ship(game->ship);
	free(game->enemy);
	free(game);
}

/*
** display.c for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
*/

#include "../includes/includes.h"

void put_title(char *title)
{
	my_putstr_color("cyan", "\n·.¸¸.··.¸¸.·\t");
	my_putstr_color("cyan", title);
	my_putstr_color("cyan", "\t·.¸¸.··.¸¸.·\n\n");
}

void put_stat(char *label, int value)
{
	my_putstr_color("blue", "\t• ");
	my_putstr_color("blue", label);
	my_putstr_color("blue", " : ");
	my_put_nbr(value);
	my_putchar('\n');
}

void put_success(char* message)
{
	my_putstr_color("green", "✔ ");
	my_putstr_color("green", message);
	my_putchar('\n');
}

void put_error(char* message)
{
	my_putstr_color("red", "✘ ");
	my_putstr_color("red", message);
	my_putchar('\n');
}

void put_info(char* message)
{
	my_putstr_color("blue", "i ");
	my_putstr_color("blue", message);
	my_putchar('\n');
}

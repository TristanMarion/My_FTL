/*
** system_repair.c for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
*/

#include "../includes/ftl.h"
#include "../includes/system_repair.h"
#include "../includes/includes.h"
#include "../includes/display.h"
#include "../includes/game.h"
#include "../includes/commands.h"

const t_repair_command repair_command_array[] = {
		{"ftl_drive", ftl_drive_system_repair},
		{"navigation_tools", navigation_tools_system_repair},
		{"weapon", weapon_system_repair},
		{NULL, NULL}
};

void ftl_drive_system_repair(t_ship *ship)
{
	my_putstr_color("blue", "Reactor repair...\n");
	free_tool(&(ship->ftl_drive->system_state));
	if ((ship->ftl_drive->system_state = my_strdup("online")) == NULL)
	{
		put_error("Reactor repair failed");
		return;
	}
	put_success("Reactor repair done! Reactors OK !");
}

void navigation_tools_system_repair(t_ship *ship)
{
	my_putstr_color("blue", "Navigation tools repair...\n");
	free_tool(&(ship->navigation_tools->system_state));
	if ((ship->navigation_tools->system_state = my_strdup("online")) == NULL)
	{
		put_error("Navigation tools repair failed");
		return;
	}
	put_success("Navigation tools repair done! Navigation tools OK !");
}

void weapon_system_repair(t_ship *ship)
{
	my_putstr_color("blue", "Weapon system repair...\n");
	free_tool(&(ship->weapon->system_state));
	if ((ship->weapon->system_state = my_strdup("online")) == NULL)
	{
		put_error("Weapon system repair failed");
		return;
	}
	put_success("Weapon system repair done! Weapons OK !");
}

void		system_repair(t_game *game)
{
	char	*command;
	int		i;

	command = NULL;
	i = 0;
	my_putstr_color("yellow", "\nWhich tool do you want to repair ? ~~> ");
	if ((command = readline()) == NULL)
	{
		put_error("Please enter a command");
		return;
	}
	while (repair_command_array[i].command != NULL)
	{
		if (my_strcmp(command, repair_command_array[i].command) == 0)
		{
			execute_repair(i, game);
			free(command);
			return;
		}
		i++;
	}
	put_error("Unknown command");
	free(command);
}

void	execute_repair(int i, t_game *game)
{
	my_putstr("\n");
	repair_command_array[i].cmd_ptr(game->ship);
	enemy_attack(game->ship, game->enemy);
	my_putstr("\n");
}

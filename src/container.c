/*
** container.c for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
*/

#include "../includes/ftl.h"
#include "../includes/bonus.h"
#include "../includes/display.h"
#include "../includes/game.h"
#include "../includes/container.h"
#include "../includes/commands.h"

const t_upgrade_command upgrade_command_array[] = {
		{"attackbonus", upgrade_attack},
		{"evadebonus", upgrade_evade},
		{"energy", add_energy},
		{NULL, NULL}
};

int				add_container_to_ship(t_ship *ship)
{
	t_container	*container;

	put_info("Adding container to ship...");
	if ((container = malloc(sizeof(t_container))) == NULL)
	{
		put_error("Could not add container to ship");
		return (0);
	}
	container->first = NULL;
	container->last = NULL;
	container->nb_elem = 0;
	ship->container = container;
	put_success("Container successfully added to ship");
	return (1);
}

void			add_freight_to_container(t_ship *ship, t_freight *freight)
{
	t_container	*container;
	t_freight	*last;

	container = ship->container;
	last = NULL;
	freight->next = NULL;
	container->nb_elem++;
	container->last = freight;
	if (container->first == NULL)
	{
		freight->prev = NULL;
		container->first = freight;
		return;
	}
	last = container->first;
	while (last->next != NULL)
		last = last->next;
	last->next = freight;
	freight->prev = last;
	container->last = freight;
}

void			del_freight_from_container(t_ship *ship, t_freight *freight)
{
	t_container	*container;

	container = ship->container;
	if (container->first == NULL || freight == NULL)
		return;
	if (container->first == freight)
		container->first = freight->next;
	if (container->last == freight)
		container->last = freight->prev;
	if (freight->next != NULL)
		freight->next->prev = freight->prev;
	if (freight->prev != NULL)
		freight->prev->next = freight->next;
	free(freight->item);
	free(freight);
	ship->container->nb_elem--;
}

void			get_bonus(t_game *game)
{
	t_freight	*freight;
	int			i;
	int			energy;
	int			evade;
	int			damage;

	freight = NULL;
	i = 0;
	energy = game->ship->ftl_drive->energy;
	evade = game->ship->navigation_tools->evade;
	damage = game->ship->weapon->damage;
	while (game->ship->container->first != NULL)
	{
		i = 0;
		freight = game->ship->container->first;
		while (upgrade_command_array[i].command != NULL)
		{
			if (my_strcmp(freight->item, upgrade_command_array[i++].command) == 0)
				upgrade_command_array[i - 1].cmd_ptr(game->ship);
		}
		del_freight_from_container(game->ship, freight);
	}
	put_gain(game->ship, energy, evade, damage);
	enemy_attack(game->ship, game->enemy);
}

void put_gain(t_ship *ship, int energy, int evade, int damage)
{
	put_title("Collected bonuses");
	put_stat("Energy (x1)", ship->ftl_drive->energy - energy);
	put_stat("Evade (x3)", (ship->navigation_tools->evade - evade) / 3);
	put_stat("Damage (x5)", (ship->weapon->damage - damage) / 5);
	put_title("New stats after bonuses");
	my_putstr_color("blue", "\t• Energy : ");
	my_put_nbr(ship->ftl_drive->energy);
	my_putstr_color("blue", " (+");
	my_put_nbr(ship->ftl_drive->energy - energy);
	my_putstr_color("blue", ")\n\t• Evade : ");
	my_put_nbr(ship->navigation_tools->evade);
	my_putstr_color("blue", " (+");
	my_put_nbr(ship->navigation_tools->evade - evade);
	my_putstr_color("blue", ")\n\t• Damage : ");
	my_put_nbr(ship->weapon->damage);
	my_putstr_color("blue", " (+");
	my_put_nbr(ship->weapon->damage - damage);
	my_putstr_color("blue", ")\n\n\n");
}

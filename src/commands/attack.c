/*
** attack.c for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
*/

#include "../../includes/game.h"
#include "../../includes/display.h"
#include "../../includes/commands.h"

void		attack(t_game *game)
{
	t_ship	*ship;

	ship = game->ship;
	if (game->enemy == NULL)
	{
		put_error("There is nothing to attack !");
		return;
	}
	if (my_strcmp(ship->weapon->system_state, "online") != 0)
	{
		put_error("Error: Broken weapons, fix them before trying an attack");
		return;
	}
	player_attack(game);
}

void			player_attack(t_game *game)
{
	t_enemy		*enemy;
	t_ship		*ship;
	int			rand;

	rand = 0;
	enemy = game->enemy;
	ship = game->ship;
	enemy->hp -= ship->weapon->damage;
	if (enemy->hp <= 0)
	{
		free(enemy);
		game->enemy = NULL;
		my_putstr("\n");
		put_success("Enemy destroyed !");
		rand = random() % 2;
		if (rand == 0 && ship->ftl_drive->energy > 0)
		{
			put_error("You lose an energy point because of the fight");
			ship->ftl_drive->energy--;
		}
		my_putstr("\n");
		return;
	}
	enemy_stats(enemy);
	enemy_attack(ship, enemy);
}

void	enemy_attack(t_ship *ship, t_enemy *enemy)
{
	int	rand;

	rand = random() % 100;
	if (enemy == NULL)
		return;
	my_putstr("\n");
	if (rand < ship->navigation_tools->evade)
	{
		put_success("You dodged the enemy attack !\n");
		return;
	}
	put_error("The enemy attack hits !");
	ship->hull -= enemy->damage;
	put_title("Ship damages");
	put_stat("Damages taken", enemy->damage);
	put_stat("Ship hull", ship->hull);
	my_putstr("\n\n");
	break_tools(ship);
}

void	break_tools(t_ship *ship)
{
	int	rand;

	rand = random() % 5;
	if (rand != 0)
	{
		put_success("No tool is touched by enemy's attack\n");
		return;
	}
	rand = random() % 3;
	if (rand == 0)
	{
		put_error("Your weapon system is broken by enemy's attack");
		free_tool(&(ship->weapon->system_state));
	}
	else if (rand == 1)
	{
		put_error("Your ftl drive is broken by enemy's attack");
		free_tool(&(ship->ftl_drive->system_state));
	}
	else
	{
		put_error("Your navigation system is broken by enemy's attack");
		free_tool(&(ship->navigation_tools->system_state));
	}
	my_putstr("\n");
}

void free_tool(char **tool)
{
	free(*tool);
	*tool = NULL;
}

/*
** stat.c for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
*/

#include "../../includes/game.h"
#include "../../includes/display.h"

void		stat(t_game *game)
{
	t_ship	*ship;

	ship = game->ship;
	put_title("Ship stats");
	put_stat("Current sector", ship->navigation_tools->sector);
	put_stat("Ship hull", ship->hull);
	put_stat("FTL drive energy", ship->ftl_drive->energy);
	put_stat("Weapon damage", ship->weapon->damage);
	put_stat("Evade percent", ship->navigation_tools->evade);
	put_stat("Container freight number", ship->container->nb_elem);
	my_putstr("\n\n");
}

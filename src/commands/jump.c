/*
** jump.c for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
*/

#include "../../includes/game.h"
#include "../../includes/commands.h"
#include "../../includes/display.h"
#include "../../includes/enemy.h"

void 		jump(t_game *game)
{
	t_ship	*ship;

	ship = game->ship;
	enemy_attack(ship, game->enemy);
	if (ship->ftl_drive->energy <= 0)
	{
		put_error("Error: Not enough energy to jump\n");
		return;
	}
	if (my_strcmp(ship->ftl_drive->system_state, "online") != 0)
	{
		put_error("Error: Broken reactor, fix it before trying a jump again\n");
		return;
	}
	if (game->enemy != NULL)
	{
		put_error("Can't jump while there is an enemy in the sector\n");
		return;
	}
	successful_jump(game);
	if (ship->navigation_tools->sector < 10)
		check_enemy(enemy_jump(game), game);
}

void		successful_jump(t_game *game)
{
	t_ship	*ship;

	ship = game->ship;
	game->detected = 0;
	ship->ftl_drive->energy--;
	ship->navigation_tools->sector++;
	put_title("Successful jump");
	put_stat("New sector", ship->navigation_tools->sector);
	put_stat("Energy left", ship->ftl_drive->energy);
	my_putstr("\n\n");
}

void check_enemy(int status, t_game *game)
{
	if (status == -1)
		put_error("Could not create enemy");
	else if (status == 0)
		put_info("The sector is clean, no enemy in sight");
	else
	{
		put_error("[RED ALERT] Enemy detected");
		enemy_stats(game->enemy);
	}
	my_putstr("\n");
}

void enemy_stats(t_enemy *enemy)
{
	put_title("Enemy stats");
	put_stat("Heal Points", enemy->hp);
	put_stat("Damage", enemy->damage);
	my_putstr("\n");
}

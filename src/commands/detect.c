/*
** detect.c for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
*/

#include "../../includes/game.h"
#include "../../includes/display.h"
#include "../../includes/container.h"
#include "../../includes/commands.h"

void	detect(t_game *game)
{
	int			i;
	t_freight	*freight;

	i = 0;
	freight = NULL;
	my_putstr("\n");
	if (game->detected == 1)
	{
		put_error("You can use the \"detect\" command only once per sector\n");
		return;
	}
	if (my_strcmp(game->ship->navigation_tools->system_state, "online") != 0)
	{
		put_error("Error: Broken detector, fix it before trying detecting\n");
		return;
	}
	put_title("Collecting freights");
	while (i++ < 10)
	{
		try_collect(game->ship, freight);
	}
	game->detected = 1;
	enemy_attack(game->ship, game->enemy);
	my_putstr("\n");
}

t_freight		*create_freight()
{
	t_freight	*freight;
	int			rand;

	freight = NULL;
	if ((freight = malloc(sizeof(t_freight))) == NULL)
		return NULL;
	rand = random() % 1000;
	if (rand > 299)
	{
		if ((freight->item = my_strdup("scrap")) == NULL)
			return NULL;
	}
	else
	{
		if ((freight->item = not_scrap(rand)) == NULL)
			return NULL;
	}
	return freight;
}

char		*not_scrap(int random)
{
	char	*item;

	item = NULL;
	if (random < 102)
		if (random < 3)
		{
			if ((item = my_strdup("scrap")) == NULL)
				return NULL;
		}
		else
		{
			if ((item = my_strdup("energy")) == NULL)
				return NULL;
		}
	else
		if (random < 201)
		{
			if ((item = my_strdup("attackbonus")) == NULL)
				return NULL;
		}
		else
			if ((item = my_strdup("evadebonus")) == NULL)
				return NULL;
	return item;
}

void collect_freight(t_ship *ship, t_freight *freight)
{
	add_freight_to_container(ship, freight);
	my_putstr_color("green", "✔ Collected freight : ");
	my_putstr(freight->item);
	my_putstr("\n");
}

void try_collect(t_ship *ship, t_freight *freight)
{
	if ((freight = create_freight()) == NULL)
		put_error("Could not collect freight");
	else
		collect_freight(ship, freight);
}

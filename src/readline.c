/*
** my_string.c for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
*/

#include    <stdlib.h>
#include    <unistd.h>

char		*readline(void)
{
	ssize_t	ret;
	char	*buff;

	if ((buff = malloc((50 + 1) * sizeof(char))) == NULL)
		return (NULL);
	if ((ret = read(0, buff, 50)) > 1)
	{
		buff[ret - 1] = '\0';
		return (buff);
	}
	free(buff);
	return (NULL);
}

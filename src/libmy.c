/*
** libmy.c for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
*/

#include "../includes/includes.h"

void my_put_nbr(int n)
{
	if (n < 0)
	{
		n = -n;
		my_putchar('-');
	}
	if (n >= 10)
	{
		my_put_nbr(n / 10);
		my_put_nbr(n % 10);
	}
	else
		my_putchar(n + 48);
}

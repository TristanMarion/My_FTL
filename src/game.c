/*
** game.c for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
*/

#include "../includes/game.h"
#include "../includes/air_shed.h"
#include "../includes/container.h"
#include "../includes/includes.h"
#include "../includes/commands.h"
#include "../includes/display.h"

const t_game_command game_command_array[] = {
		{"jump", jump},
		{"stat", stat},
		{"repair_system", system_repair},
		{"control_system", system_control},
		{"detect", detect},
		{"get_bonus", get_bonus},
		{"attack", attack},
		{NULL, NULL}
};

void		game()
{
	t_game	*game;

	my_putstr_color("clear", "Welcome to the Nebuchadnezzar !\n\n");
	if ((game = create_game()) == NULL)
	{
		put_error("Could not create a new game");
		return;
	}
	while (game->finished != 1)
	{
		play(game);
		update_game_status(game);
	}
	end_game(game);
	free_game(game);
}

t_game		*create_game()
{
	t_game	*game;

	if ((game = malloc(sizeof(t_game))) == NULL)
		return (NULL);
	if ((game->ship = create_ship()) == NULL)
		return (NULL);
	if (add_weapon_to_ship(game->ship) == 0)
		return (NULL);
	if (add_ftl_drive_to_ship(game->ship) == 0)
		return (NULL);
	if (add_navigation_tools_to_ship(game->ship) == 0)
		return (NULL);
	if (add_container_to_ship(game->ship) == 0)
		return (NULL);
	game->detected = 0;
	game->finished = 0;
	game->enemy = NULL;
	my_putstr("\n");
	return (game);
}

void		play(t_game *game)
{
	char	*command;
	int 	i;

	command = NULL;
	i = 0;
	my_putstr_color("yellow", "What do you want to do ? ~~> ");
	if ((command = readline()) == NULL)
	{
		put_error("Please enter a command");
		return;
	}
	while (game_command_array[i].command != NULL)
	{
		if (my_strcmp(command, game_command_array[i].command) == 0)
		{
			game_command_array[i].cmd_ptr(game);
			free(command);
			return;
		}
		i++;
	}
	free(command);
	put_error("Unknown command");
}

void update_game_status(t_game *game)
{
	if (game->ship->navigation_tools->sector == 10 ||
			(game->ship->ftl_drive->energy == 0 &&
			game->ship->container->nb_elem == 0 &&
			game->detected == 1) ||
			 game->ship->hull <= 0)
		game->finished = 1;
}

void end_game(t_game *game)
{
	if (game->ship->navigation_tools->sector == 10)
		put_success("Congrats ! You reached the HQ !");
	else if (game->ship->ftl_drive->energy == 0)
		put_error("You lost, no more energy to end your journey");
	else
		put_error("You lost, your ship exploded");
}

/*
** air_shed.c for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
*/

#include "../includes/ftl.h"
#include "../includes/display.h"

t_ship		*create_ship()
{
	t_ship	*ship;

	put_info("Starting ship construction...");
	if ((ship = malloc(sizeof(t_ship))) == NULL)
	{
		put_error("Not enough materials for building the ship");
		return (NULL);
	}
	ship->hull = 50;
	ship->weapon = NULL;
	ship->ftl_drive = NULL;
	ship->navigation_tools = NULL;
	ship->container = NULL;
	put_success("Ship construction done !");
	return (ship);
}

int				add_weapon_to_ship(t_ship *ship)
{
	t_weapon	*weapon;

	put_info("Adding weapons to ship...");
	if ((weapon = malloc(sizeof(t_weapon))) == NULL)
	{
		put_error("Could not add weapons to ship");
		return (0);
	}
	weapon->damage = 10;
	if ((weapon->system_state = my_strdup("online")) == NULL)
	{
		put_error("Could not add weapons to ship");
		return (0);
	}
	ship->weapon = weapon;
	put_success("Weapons successfully added to ship !");
	return (1);
}

int				add_ftl_drive_to_ship(t_ship *ship)
{
	t_ftl_drive	*ftl_drive;

	put_info("Adding reactors to ship...");
	if ((ftl_drive = malloc(sizeof(t_ftl_drive))) == NULL)
	{
		put_error("Could not add reactors to ship");
		return (0);
	}
	ftl_drive->energy = 10;
	if ((ftl_drive->system_state = my_strdup("online")) == NULL)
	{
		put_error("Could not add reactors to ship");
		return (0);
	}
	ship->ftl_drive = ftl_drive;
	put_success("Reactors successfully added to ship !");
	return (1);
}

int						add_navigation_tools_to_ship(t_ship *ship)
{
	t_navigation_tools	*navigation_tools;

	put_info("Adding navigation tools...");
	if ((navigation_tools = malloc(sizeof(t_navigation_tools))) == NULL)
	{
		put_error("Could not add navigation tools to ship");
		return (0);
	}
	navigation_tools->sector = 0;
	navigation_tools->evade = 25;
	if ((navigation_tools->system_state = my_strdup("online")) == NULL)
	{
		put_error("Could not add navigation tools to ship");
		return (0);
	}
	ship->navigation_tools = navigation_tools;
	put_success("Navigation tools successfully added to ship!");
	return (1);
}

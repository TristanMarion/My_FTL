/*
** bonus.c for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
*/

#include "../includes/ftl.h"
#include "../includes/bonus.h"

void upgrade_attack(t_ship *ship)
{
	ship->weapon->damage += 5;
}

void upgrade_evade(t_ship *ship)
{
	ship->navigation_tools->evade += 3;
}

void add_energy(t_ship *ship)
{
	ship->ftl_drive->energy += 1;
}


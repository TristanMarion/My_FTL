/*
** system_control.c for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
*/

#include "../includes/ftl.h"
#include "../includes/game.h"
#include "../includes/display.h"

void ftl_drive_system_check(t_ship *ship)
{
	my_putstr_color("blue", "Checking reactor...\n");
	if (my_strcmp(ship->ftl_drive->system_state, "online") == 0)
		put_success("Reactors OK !");
	else
		put_error("Reactors KO !");
}

void navigation_tools_system_check(t_ship *ship)
{
	my_putstr_color("blue", "Checking navigation system...\n");
	if (my_strcmp(ship->navigation_tools->system_state, "online") == 0)
		put_success("Navigation system OK !");
	else
		put_error("Navigation system KO !");
}

void weapon_system_check(t_ship *ship)
{
	my_putstr_color("blue", "Checking weapon system...\n");
	if (my_strcmp(ship->weapon->system_state, "online") == 0)
		put_success("Weapon system OK !");
	else
		put_error("Weapon system KO !");
}

void system_control(t_game *game)
{
	my_putstr("\n");
	ftl_drive_system_check(game->ship);
	navigation_tools_system_check(game->ship);
	weapon_system_check(game->ship);
	my_putstr("\n");
}

/*
** system_repair.h for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
*/

#ifndef MY_FTL_SYSTEM_REPAIR_H
#define MY_FTL_SYSTEM_REPAIR_H

#include "game.h"

void	ftl_drive_system_repair(t_ship *ship);
void	navigation_tools_system_repair(t_ship *ship);
void	weapon_system_repair(t_ship *ship);
void	system_repair(t_game *game);
void	execute_repair(int i, t_game *game);

#endif /* MY_FTL_SYSTEM_REPAIR_H */

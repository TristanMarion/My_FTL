/*
** container.h for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
**
*/

#ifndef MY_FTL_CONTAINER_H
#define MY_FTL_CONTAINER_H

int		add_container_to_ship(t_ship *ship);
void	add_freight_to_container(t_ship *ship, t_freight *freight);
void	del_freight_from_container(t_ship *ship, t_freight *freight);
void	get_bonus(t_game *game);
void	put_gain(t_ship *ship, int energy, int evade, int damage);

#endif /* MY_FTL_CONTAINER_H */

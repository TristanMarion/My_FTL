/*
** display.h for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
*/

#ifndef MY_FTL_DISPLAY_H
#define MY_FTL_DISPLAY_H

void put_title(char *title);
void put_stat(char *label, int value);
void put_success(char *title);
void put_error(char *title);
void put_info(char* message);

#endif /* MY_FTL_DISPLAY_H */

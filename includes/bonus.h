/*
** bonus.h for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
*/

#ifndef MY_FTL_BONUS_H
#define MY_FTL_BONUS_H

typedef struct s_upgrade_command
{
	char	*command;
	void	(*cmd_ptr)(t_ship *);
}	t_upgrade_command;

void	upgrade_attack(t_ship *ship);
void	upgrade_evade(t_ship *ship);
void	add_energy(t_ship *ship);

#endif /* MY_FTL_BONUS_H */

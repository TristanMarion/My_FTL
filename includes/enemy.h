/*
** enemy.h for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
*/

#ifndef MY_FTL_ENEMY_H
#define MY_FTL_ENEMY_H

typedef struct s_enemy
{
	int damage;
	int	hp;
}	t_enemy;

t_enemy	*create_enemy();

#endif /* MY_FTL_ENEMY_H */

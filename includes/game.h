/*
** game.h for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
*/

#ifndef MY_FTL_GAME_H
#define MY_FTL_GAME_H

#include "ftl.h"
#include "enemy.h"

typedef struct s_game
{
	t_ship	*ship;
	t_enemy	*enemy;
	int		detected;
	int		finished;
}   t_game;

void	game();
t_game	*create_game();
void	play(t_game *game);
void	update_game_status(t_game *game);
void	end_game(t_game *game);
void	free_ship(t_ship *ship);
void	free_game(t_game *game);

#endif /* MY_FTL_GAME_H */

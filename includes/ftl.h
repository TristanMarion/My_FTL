/*
** ftl.h for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
**
*/

#ifndef FTL_H_
# define FTL_H_

#include <stdlib.h>
#include <time.h>
#include "includes.h"

typedef struct s_weapon
{
	char	*system_state;
	int		damage;
}   t_weapon;

typedef struct s_ftl_drive
{
	char	*system_state;
	int		energy;
}   t_ftl_drive;

typedef struct s_navigation_tools
{
	char	*system_state;
	int		sector;
	int		evade;
}   t_navigation_tools;

typedef struct s_freight
{
	char				*item;
	struct s_freight	*next;
	struct s_freight	*prev;
}   t_freight;

typedef struct s_container
{
	t_freight	*first;
	t_freight	*last;
	int			nb_elem;
}   t_container;

typedef struct s_ship
{
	int					 hull;
	t_weapon			*weapon;
	t_ftl_drive			*ftl_drive;
	t_navigation_tools	*navigation_tools;
	t_container			*container;
}   t_ship;

typedef struct s_repair_command
{
	char	*command;
	void	(*cmd_ptr)(t_ship *);
}	t_repair_command;

#endif /* FTL_H */

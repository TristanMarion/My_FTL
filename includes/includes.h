/*
** includes.h for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
*/

#ifndef MY_FTL_INCLUDES_H
#define MY_FTL_INCLUDES_H

void	my_putchar(const char c);
void	my_putstr(const char *str);
char	*my_strdup(const char *str);
int		my_strcmp(const char *s1, const char *s2);
char	*readline(void);
void	my_putstr_color(const char *color, const char *str);
void	my_put_nbr(int n);

#endif /* MY_FTL_INCLUDES_H */

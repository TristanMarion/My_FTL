/*
** commands.h for My_FTL in /home/marion_t/My_FTL For My_FTL
**
** Made by MARION Tristan
** Login   <marion_t@etna-alternance.net>
**
** Started on  Mon Nov  6 11:19:36 2017 MARION Tristan
** Last update Mon Nov  6 11:10:41 2017 MARION Tristan
*/

#ifndef MY_FTL_COMMANDS_H
#define MY_FTL_COMMANDS_H

#include "game.h"
#include "enemy.h"

typedef struct s_game_command
{
	char	*command;
	void	(*cmd_ptr)(t_game *);
}   t_game_command;

void	jump(t_game *game);
void	successful_jump(t_game *game);
void 	check_enemy(int status, t_game *game);
void	enemy_stats(t_enemy *enemy);
int		enemy_jump(t_game *game);

void	stat(t_game *game);

void	system_repair(t_game *game);

void	system_control(t_game *game);

void		detect(t_game *game);
void		try_collect(t_ship *ship, t_freight *freight);
t_freight	*create_freight();
char		*not_scrap(int random);
void		collect_freight(t_ship *ship, t_freight *freight);

void		get_bonus(t_game *game);

void		attack(t_game *game);
void		player_attack(t_game *game);
void		enemy_attack(t_ship *ship, t_enemy *enemy);
void		break_tools(t_ship *ship);
void		free_tool(char **tool);

#endif /* MY_FTL_COMMANDS_H */
